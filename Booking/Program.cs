using System;
using System.Collections.Generic;

public class Program
{
    public static void booking(int hour, int minutes, ReservedMinutes[] arr)
    {
        ReservedMinutes reservedMinutes = arr[hour];
        if (minutes < 30)
        {
            if (minutes < reservedMinutes.reserveUntilThirty)
            {
                Console.WriteLine("Already reserved");
            }
            else
            {
                reservedMinutes.reserveUntilThirty = minutes;
                Console.WriteLine("Reserved successfully");
            }
        }
        else
        {
            if (minutes < reservedMinutes.reserveAfterThirty)
            {
                Console.WriteLine("Already reserved");
            }
            else
            {
                reservedMinutes.reserveAfterThirty = minutes;
                Console.WriteLine("Reserved successfully");
            }
        }
    }

    public static void Main(string[] args)
    {
        ReservedMinutes[] reserveArr = new ReservedMinutes[24];
        for (int i = 0; i < 24; ++i)
        {
            reserveArr[i] = new ReservedMinutes();
        }

        Console.WriteLine("Enter x for exit");
        while (true)
        {
            Console.WriteLine("Enter hour:");
            string input = Console.ReadLine();
            if (input.Trim().Equals("x"))
            {
                break;
            }

            int hour = int.Parse(input);
            if (hour is < 0 or > 23)
            {
                Console.WriteLine("Incorrect hour");
                continue;
            }

            Console.WriteLine("Enter minutes:");
            int minutes = int.Parse(Console.ReadLine());
            if (minutes is < 0 or > 59) 
            {
                Console.WriteLine("Invalid minutes");
                continue;
            }

            booking(hour, minutes, reserveArr);
        }
    }
}

public class ReservedMinutes
{
    public int reserveUntilThirty { get; set; }
    public int reserveAfterThirty { get; set; }

    public ReservedMinutes()
    {
        this.reserveUntilThirty = 0;
        this.reserveAfterThirty = 30;
    }
}